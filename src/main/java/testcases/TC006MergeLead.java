package testcases;



import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC006MergeLead extends ProjectMethods {

	@BeforeTest
	public void setData() {
		testCaseName = "TC006_MergeLead";
		testDesc = "Merge A new Lead";
		author = "niloy";
		category = "smoke";
		logger = extent.createTest("TC006MergeLead");
	}
	
	@Test
	public void mergeLead() throws InterruptedException {
		
	WebElement eleCRMSFA = locateElement("partialLinkedText", "CRM/SFA");
	click(eleCRMSFA);
	click(locateElement("linkedText", "Leads"));
	click(locateElement("linkedText", "Merge Leads"));
	
	/*click(locateElement("xpath", "//img[@alt='Lookup']"));
	
	switchToWindow(1);
	WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])");
	type(firstName, "Check11");
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	Thread.sleep(3000);
	click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	//switchToWindow(0);
	click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
	switchToWindow(2);
	//click(locateElement("linkedText", "Find Leads"));
	WebElement firstName1 = locateElement("xpath", "(//input[@name='firstName'])");
	type(firstName1, "Check12");
	click(locateElement("xpath", "//button[text()='Find Leads']"));
	click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	switchToWindow(0);
	
	
	*/
	
	
	click(locateElement("xpath","(//input[@id='ComboBox_partyIdFrom']/following::a)[1]"));
	String windowHandle = driver.getWindowHandle();
	switchToWindow(1);
			type(locateElement("xpath","//input[@name=\"firstName\"]"), "Tapan");
			click(locateElement("xpath","//button[text()='Find Leads']"));
			Thread.sleep(3000);
			String leadId1 = getText(locateElement("xpath", "//a[@class=\"linktext\"][1]"));
			try {
				click(locateElement("xpath", "//a[@class=\"linktext\"][1]"));
			} catch (Exception e) {
				driver.switchTo().window(windowHandle);
			}

			click(locateElement("xpath", "(//input[@id=\"ComboBox_partyIdTo\"]/following::a)[1]"));

			Thread.sleep(3000);
			
			Set<String> windowHandles = driver.getWindowHandles();
			for (String string : windowHandles) {
				if (!string.equalsIgnoreCase(windowHandle))
			driver.switchTo().window(string);
			}
			
			type(locateElement("xpath","//input[@name=\"firstName\"]"), "Tapan");
			click(locateElement("xpath","//button[text()='Find Leads']"));
			Thread.sleep(3000);
			String leadId2 = getText(locateElement("xpath", "(//a[@class=\"linktext\"])[6]"));
			
			try {
				click(locateElement("xpath", "(//a[@class=\"linktext\"])[6]"));				
				
			} catch (Exception e) {
				driver.switchTo().window(windowHandle);
			}
			
		
			Thread.sleep(4000);

	     	System.out.println("merging leads "+leadId1 +" with "+ leadId2) ;
	     	
			try {
				click(locateElement("linkedText","Merge"));
				
			} catch (Exception e) {
				Thread.sleep(6000);
				acceptAlert();
				Thread.sleep(10000);
			}
			
	
	
	

}
	
}
