package testcases;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class ExtentReportGenerate {
    
	@Test
	public void report() throws IOException  {
		
		ExtentHtmlReporter html=new ExtentHtmlReporter("./report/result.html");
		html.setAppendExisting(false);
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(html);
		
		ExtentTest logger=extent.createTest("TC001_CreateLead", "Create a new Lead");
		logger.assignAuthor("Niloy");
		logger.assignCategory("Sanity");
		logger.log(Status.PASS, "The Data DemoSalesmanager entered successfully", MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		
		extent.flush();
	}

}
