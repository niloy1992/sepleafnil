package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC003EditLead extends ProjectMethods {
	@BeforeTest(groups= {"sanity"})
	public void setData() {
		testCaseName = "TC003_EditLead";
		testDesc = "Create A new Lead";
		author = "niloy";
		category = "sanity";
		logger = extent.createTest("TC003EditLead");
	}
	
	
	//(dependsOnMethods= {"testcases.TC002CreateLead.createLead"})
	//@Test(groups= {"sanity"})
	@Test(dataProvider="qa")
	public void editLead(String fName,String CName,String Lname,String industry,String ownership) {
		
		WebElement eleCRMSFA = locateElement("partialLinkedText", "CRM/SFA");
		click(eleCRMSFA);
		click(locateElement("linkedText", "Leads"));
		click(locateElement("linkedText", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		click(locateElement("linkedText", "Edit"));
		clear(locateElement("id", "updateLeadForm_companyName"));
		type(locateElement("updateLeadForm_companyName"), CName);
		clear(locateElement("id", "updateLeadForm_lastName"));
		type(locateElement("updateLeadForm_lastName"), Lname);
		selectDropDownUsingText(locateElement("name", "industryEnumId"), industry);
		selectDropDownUsingText(locateElement("name", "ownershipEnumId"), ownership);
		
		click(locateElement("xpath", "//img[@id='updateLeadForm_birthDate-button']"));
		
		click(locateElement("xpath", "//input[@value='Update']"));
     }
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException{
		
		Object[][] data= ReadExcel.readExcel();
	
	return data;
	
}
	
}
