package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import excel.ReadExcel;
import wdMethods.ProjectMethods;

public class TC002CreateLead extends ProjectMethods{
	
	@BeforeTest(groups= {"smoke"})
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDesc = "Create A new Lead";
		author = "niloy";
		category = "sanity";
		logger = extent.createTest("TC002CreateLead");
	}

	
	@Test(dataProvider="qa")
	//@Test(groups= {"smoke"})
	//(String cName,String fName,String Lname)
	public void createLead(String cName,String fName,String Lname,String email,String phn) {
		
		WebElement eleCRMSFA = locateElement("partialLinkedText", "CRM/SFA");
		click(eleCRMSFA);
		click(locateElement("linkedText", "Leads"));
		click(locateElement("linkedText", "Create Lead"));
		click(locateElement("createLeadForm_companyName"));
		type(locateElement("createLeadForm_companyName"), cName);
		click(locateElement("createLeadForm_firstName"));
		type(locateElement("createLeadForm_firstName"), fName);
		click(locateElement("createLeadForm_lastName"));
		type(locateElement("createLeadForm_lastName"), Lname);
		type(locateElement("createLeadForm_primaryEmail"), email);
		type(locateElement("createLeadForm_primaryPhoneNumber"), phn);
		click(locateElement("class", "smallSubmit"));
			
	}
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException{
		
		Object[][] data= ReadExcel.readExcel();
		
//		data[0][0]="CTS";
//		data[0][1]="Niloy";
//		data[0][2]="B";
//		
//		
//		data[1][0]="CTS1";
//		data[1][1]="Nilo1y";
//		data[1][2]="B";
		
		return data;
		
	}
	

	
}












