package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC005DuplicateLead extends ProjectMethods {
	
	@BeforeTest(groups= {"regression"})
	public void setData() {
		testCaseName = "TC004_DeleteLead";
		testDesc = "Create A new Lead";
		author = "niloy";
		category = "sanity";
		logger = extent.createTest("TC004DeleteLead");
	}
	@Test(dataProvider="qa")
	//@Test
	public void duplicateLead(String fName,String CName,String Lname,String industry,String ownership) {
		
		WebElement eleCRMSFA = locateElement("partialLinkedText", "CRM/SFA");
		click(eleCRMSFA);
		click(locateElement("linkedText", "Leads"));
		click(locateElement("linkedText", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		click(locateElement("linkedText", "Duplicate Lead"));
		clear(locateElement("id", "createLeadForm_companyName"));
		type(locateElement("createLeadForm_companyName"), CName);
		clear(locateElement("id", "createLeadForm_lastName"));
		type(locateElement("createLeadForm_lastName"), Lname);
		selectDropDownUsingText(locateElement("name", "industryEnumId"), industry);
		selectDropDownUsingText(locateElement("name", "ownershipEnumId"), ownership);
		click(locateElement("xpath", "//input[@value='Create Lead']"));
     }
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException{
		
		Object[][] data= ReadExcel.readExcel();
	
	return data;
	
}
	
	
	
}
