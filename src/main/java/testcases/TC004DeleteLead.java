package testcases;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import excel.ReadExcel;
import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC004DeleteLead extends ProjectMethods {
	
	@BeforeTest(groups= {"regression"})
	public void setData() {
		testCaseName = "TC004_DeleteLead";
		testDesc = "Create A new Lead";
		author = "niloy";
		category = "sanity";
		logger = extent.createTest("TC004DeleteLead");
	}
	
	//@Test(groups= {"regression"})
	@Test(dataProvider="qa")
	public void deleteLead(String fName) {
		click(locateElement("partialLinkedText", "CRM/SFA"));
		click(locateElement("linkedText", "Leads"));
		click(locateElement("linkedText", "Find Leads"));
		type(locateElement("xpath", "(//input[@name='firstName'])[3]"), fName);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		click(locateElement("linkedText", "Delete"));
}
	
	@DataProvider(name="qa")
	public Object[][] fetchData() throws IOException{
		
		Object[][] data= ReadExcel.readExcel();
	
	return data;
	
}
}
