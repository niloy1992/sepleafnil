package excel;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.Test;

public class ReadExcel {
	
	public static Object[][] readExcel() throws IOException  {
		
		XSSFWorkbook  wb=new XSSFWorkbook("./data/editLead.xlsx");
				
		XSSFSheet sheet=wb.getSheetAt(0);  //go to sheet
		
		int rowCount=sheet.getLastRowNum(); //rows count
		
		int cellCount=sheet.getRow(0).getLastCellNum();     //column count
		
		Object[][] data =new Object[rowCount][cellCount];
		
		for(int j=1;j<=rowCount;j++) {  //if j=1 it starts with values and not with the header but if j=0 then it starts with header
			
			XSSFRow row=sheet.getRow(j); //go to specific row
			
			
			for(int i=0;i<cellCount;i++) {
				
				XSSFCell cell=row.getCell(i);
				try {
					String value = cell.getStringCellValue();
					
					data[j-1][i]=value;
					
					System.out.println(value);
				} catch (NullPointerException e) {
					System.out.println("");
				}
			}
		}
		
		
		wb.close();
		return data;
		
	}

}
