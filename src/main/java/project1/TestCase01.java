package project1;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TestCase01 extends SeMethods {
	


	@Test
	public void getPrice() throws InterruptedException {

		startApp("chrome", "https://www.zoomcar.com/chennai/");
		click(locateElement("linkedText", "Start your wonderful journey"));
		click(locateElement("xpath", " (//div[@class='component-popular-locations'])/div/following-sibling::div"));
		click(locateElement("xpath", "//button[@class='proceed']"));

		Date date=new Date();
		DateFormat sdf=new SimpleDateFormat("dd");
		String today=sdf.format(date);

		System.out.println(today);

		//div[contains(text(),'14')]

		int tomorrow=Integer.parseInt(today) + 1;
		click(locateElement("xpath", "(//div[contains(text(),'"+tomorrow+"')])"));

		click(locateElement("xpath", "//button[@class='proceed']"));



		click(locateElement("xpath", "(//div[contains(text(),'"+tomorrow+"')])"));

		click(locateElement("xpath", "//button[@class='proceed']"));
		Thread.sleep(3000);
		List<WebElement> ent = driver.findElementsByXPath("//div[@class='selectedLocation']");

		System.out.println(ent.size());

		List<Integer> list2=new ArrayList<>();
		Thread.sleep(3000);
		List<WebElement> list=driver.findElementsByXPath("//div[@class='price']");
		System.out.println(list.size());
		for (WebElement each : list) {
			String replace = each.getText().replaceAll("\\D", "");
			int parseInt = Integer.parseInt(replace);
			list2.add(parseInt);
		}
		System.out.println(list2);

		System.out.println(Collections.max(list2));



		//Collections.sort(list);

		//click(locateElement("xpath", "//div[@class='day']/div"));



	}

}
