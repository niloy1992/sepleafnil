package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
//				ChromeOptions obj=new ChromeOptions();
//				obj.addArguments("--disable-notifications");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")) {			
				System.setProperty("webdriver.gecko.driver", "./driver/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("pass", "The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			
			//System.out.println("The Browser "+browser+" not Launched ");
		reportStep("fail", "The Browser "+browser+" not Launched ");
		} 
		
		finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "partialLinkedText": return driver.findElementByPartialLinkText(locValue);
			case "linkedText": return driver.findElementByLinkText(locValue);
			case "name":return driver.findElementByName(locValue);
			
			}
		} catch (NoSuchElementException e) {
			System.out.println("The Element Is Not Located ");
		}
		
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		
		try {
			ele.sendKeys(data);
			//System.out.println("The Data "+data+" is Entered Successfully");
			reportStep("pass", "The Data "+data+" is Entered Successfully");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			reportStep("fail", "The Data "+data+" Not Entered");
		}
		takeSnap();
	}
	
	public void clear(WebElement ele) {
		
		ele.clear();
		System.out.println("The element is cleared successfully");
	}

	@Override
	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
		takeSnap();
	}
	
	public void clickWitOutSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" Clicked Successfully");
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
	}
	
	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByValue(value);
		System.out.println("The DropDown Is Selected with "+value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		
		Select dd = new Select(ele);
		dd.selectByIndex(index);
		System.out.println("The DropDown Is Selected with "+index);
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		
		if(driver.getTitle().equals(expectedTitle))
			return true;
		// TODO Auto-generated method stub
		else
			
		return false;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		
		if(ele.getText().equals(expectedText)){
			
			System.out.println("The text exactly matches with the expectedText");
		}
		
		else
			
			System.out.println("The text does not exactly match with the expectedText");

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		
		if(ele.getText().contains(expectedText)) {
			
			System.out.println("The text is partially available in the expected Text");
		}
		
		else
			
			System.out.println("The text is not partially available in the expected Text");
			
			

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
		if(ele.getAttribute(attribute).equals(value)) {
			
			System.out.println("The attribute is exactly matched with the given attribute");
		}
		else
		
		System.out.println("The attribute is not exactly matched with the given attribute");

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
         if(ele.getAttribute(attribute).contains(value)) {
			
			System.out.println("The attribute is partially matched with the given attribute");
		}
		else
		
		System.out.println("The attribute is not partially matched with the given attribute");

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		
		if(ele.isSelected()) {
			
			System.out.println("The element is selected");
		}
		
		else
			
			System.out.println("The element is not selected");

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		
		if(ele.isDisplayed()) {
			
			System.out.println("The element is visible");
		}
		
		else
			
			System.out.println("The element is not visible");

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		try {
		driver.switchTo().frame(ele);
		}
		
		catch(NoSuchFrameException e) {
			
			System.out.println("The frame is not located");
			
		}
		
		finally {
		takeSnap();
		
		}

	}

	@Override
	public void acceptAlert() {
		
		try {
		driver.switchTo().alert().accept();
		}
		
		catch(NoAlertPresentException e) {
			
			System.out.println("The alert is not available");
		}
		
		finally {
			
		takeSnap();
		
		}
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		driver.switchTo().alert().dismiss();
		takeSnap();

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return driver.switchTo().alert().getText();
		
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File(".report/snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		// TODO Auto-generated method stub
		
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub
		
		driver.quit();

	}

}
