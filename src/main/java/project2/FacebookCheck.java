package project2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import wdMethods.SeMethods;

public class FacebookCheck extends SeMethods{
	
	@Test
	public void facebook() throws InterruptedException {
		
		startApp("chrome", "https://www.facebook.com/");
		//click(locateElement("xpath","//input[@data-testid='royal_email']"));
		type(locateElement("email"),"niloydo@gmail.com");
		//click(locateElement("xpath","//input[@data-testid='royal_pass']"));
		type(locateElement("pass"),"NILOYBONI1992");
		locateElement("xpath", "//input[@value='Log In']").click();
		Thread.sleep(10000);
		type(locateElement("xpath", "//input[@data-testid='search_input']"), "TestLeaf");
		Thread.sleep(10000);
		click(locateElement("xpath", "//button[@data-testid='facebar_search_button']"));
		
		verifyExactText(locateElement("xpath", " //button[text()='Liked']"), "Liked");
		
		click(locateElement("xpath", "//div[text()='TestLeaf']"));
		
		Thread.sleep(10000);
		click(locateElement("xpath", "//span[text()='Reviews']"));
		Thread.sleep(10000);
		List<WebElement> locateElement = driver.findElementsByXPath("//div[@class='_1dwg _1w_m _q7o']");
		System.out.println(locateElement.size());
	}

}
