package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandle {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElementByLinkText("AGENT LOGIN").click();
		
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		
		List<String> lst1=new ArrayList<>();
		
		lst1.addAll(allWindows);
		driver.switchTo().window(lst1.get(1));
		System.out.println(driver.getTitle());
		driver.switchTo().window(lst1.get(0));
		//driver.close();
		//driver.switchTo().window(lst1.get(1));
		
		
		File src=driver.getScreenshotAs(OutputType.FILE);
		File obj=new File("./snaps/img3.jpeg");
		
		FileUtils.copyFile(src, obj);
		
		
		
		

	}

}
