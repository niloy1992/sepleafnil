package week4.day1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AlertFrameCheck {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement frame1 = driver.findElementByXPath("(//iframe)[2]");
		
		driver.switchTo().frame(frame1);
		
		driver.findElement(By.xpath("//button[text()='Try it']")).click();
		//driver.findElementByXPath().click();
		
		
		//driver.switchTo().defaultContent();
		driver.switchTo().alert().sendKeys("Niloy");
		driver.switchTo().alert().accept();
		
		WebElement elementName = driver.findElementByXPath("//p[@id='demo']");
		
		String text=elementName.getText();
		
		System.out.println(text);
		
		if(text.contains("Hello Niloy! How are you today?")) {
			
			System.out.println("Text is found");
		}
		
		else
			System.out.println("Text Not found");
		
		

	}

}
