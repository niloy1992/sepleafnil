package week2.day1;

import java.util.Scanner;

public class PrimeNumberWithSizeInput {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int g=0;
		int sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size");
		
		int l1=sc.nextInt();
		
		
		int[] arr=new int[l1];
		for(int u=0;u<l1;u++) {
			
			arr[u]= sc.nextInt();
		}
		
		for(int i=0;i<l1;i++) {
			
			for(int j=2;j<=arr[i]/2;j++) {
				
				if(arr[i]%j==0) {
					
					g=1;
					break;
				}
			}
			
			if(g==0) {
				
				sum+=arr[i];
			}
			
			g=0;
		}
		System.out.println(sum);

	}

}
