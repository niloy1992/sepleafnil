package week2.day1;

import java.util.Scanner;

public class PrimeNumberWithLimits {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int p=0;
		int sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the lower limit");
		int l1=sc.nextInt();
		System.out.println("Enter the upper limit");
		int l2=sc.nextInt();
		
		for(int k=l1;k<=l2;k++) {
			
			for(int i=2;i<=k/2;i++) {
				
				if(k%i==0) {
					
					p=1;
					break;
					
				}
			}
			if(p==0) {
				
				sum+=k;
				
			}
			
			p=0;
		}
		
		System.out.println(sum);

	}

}
