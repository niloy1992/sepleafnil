package week2.day2;

import java.util.Scanner;

public class SumArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array");
		int num=sc.nextInt();
		int arr[]=new int[num];
		System.out.print("Enter the values: ");
		for(int g=0;g<num;g++) {
			
			 arr[g]=sc.nextInt();
		}
		sc.close();
		
		for(int k=0;k<num;k++) {
			
			sum=sum+arr[k];
		}
		
		System.out.println(sum);

	}

}
