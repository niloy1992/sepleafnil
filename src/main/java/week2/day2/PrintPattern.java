package week2.day2;

import java.util.Scanner;

public class PrintPattern {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc=new Scanner(System.in);
		System.out.print("Input Numbers : ");
		int inp1=sc.nextInt();
		int inp2=sc.nextInt();
		sc.close();
		if(inp1>inp2) {
			
			System.out.println("Enter the valid input");
		}
		
		else {
			
			for(int j=inp1;j<=inp2;j++) {
				
                if((j%3==0 ) && (j%5==0)) {
					
					System.out.print("FIZZBUZZ ");
					
					
                }
				
                else if(j%3==0) {
					
					System.out.print("FIZZ ");
					
				}
				
                else if(j%5==0) {
					
					System.out.print("BUZZ ");
					
				}
				
				
				
                else if(!((j%3==0 ) || (j%5==0))) {
					System.out.print(j +" ");
					
					
				}
			}
			
		}

	}

}
