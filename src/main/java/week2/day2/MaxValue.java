package week2.day2;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections4.map.HashedMap;

public class MaxValue {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	Map<String,Integer> mobile=new HashedMap<>();
	
	mobile.put("Samsung", 5000);
	mobile.put("Apple", 6000);
	mobile.put("Redmi", 3000);
	mobile.put("Oppo", 5000);
	
	int max=0;
	for(Integer mo: mobile.values()) {
		
		if(mo>max) {
			
			max=mo;
		}
	}
	
	System.out.println(max);
	
	for(Entry<String, Integer> eachMobile: mobile.entrySet()) {
		
		if(max==eachMobile.getValue()) {
			
			System.out.println(eachMobile.getKey() + "--->" + eachMobile.getValue());
		}
		
		
	}

	}

}
