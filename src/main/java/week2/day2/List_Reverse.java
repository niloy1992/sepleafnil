package week2.day2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class List_Reverse {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		List<Integer> num=new ArrayList<>();
		num.add(4);
		num.add(6);
		num.add(7);
		num.add(2);
		num.add(1);
		System.out.println(num);
		
//		Set<Integer> st=new TreeSet<>();
//		st.addAll(num);
		Collections.sort(num);
		System.out.println(num);
		
		Collections.reverse(num);
		
		System.out.println(num);
		//System.out.println();

	}

}
