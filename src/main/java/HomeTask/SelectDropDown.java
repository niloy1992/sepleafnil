package HomeTask;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectDropDown {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		int c=0;
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		WebElement country=driver.findElementById("userRegistrationForm:countries");
		
		Select sel=new Select(country);
		
		sel.selectByVisibleText("Egypt");
		
		List<WebElement> le=sel.getOptions();
		
	    for (WebElement ele : le) {
		
	
			
			if(ele.getText().startsWith("E")) {
				
				System.out.println(ele.getText());
			}
			
		}
	    
	    

	}

}
