package HomeTask;

import java.util.Scanner;

public class LeapYear {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int k=0;
		Scanner sc=new Scanner(System.in);
		
		int year=sc.nextInt();
		
		if(year%4==0) {
			
			if(year%100==0) {
				
				if(year%400==0) {
					
					k=1;
										
				}
				
				else
					k=0;
			}
			else
				k=1;
		}
		else
			k=0;
		
		if(k==1) {
			
			System.out.println("The year is a leap year");
		}
		else
			
			System.out.println("The year is not a leap year");

	}

}
