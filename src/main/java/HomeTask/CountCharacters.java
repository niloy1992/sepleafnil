package HomeTask;

import java.text.DecimalFormat;
import java.util.Scanner;

public class CountCharacters {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int countUpper=0;
		int countLower=0;
		int countDigits=0;
		int countOtherchar=0;
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		sc.close();
		float strlen=str.length();
		for(int k=0;k<strlen;k++) {
			
			if(str.charAt(k)>='A' && str.charAt(k)<='Z')
				
				countUpper++;
			
			else if(str.charAt(k)>='a' && str.charAt(k)<='z')
				
				countLower++;
            
			else if(str.charAt(k)>='0' && str.charAt(k)<='9')
				
				countDigits++;
			
			else
				
				countOtherchar++;
			
		}
		
		System.out.println("Number of uppercase letters is "+countUpper+". So percentage is " +(countUpper*100/strlen)+"%");
		System.out.println("Number of lowercase letters is "+countLower+". So percentage is " +(countLower*100/strlen)+"%");
		System.out.println("Number of digits is "+countDigits+". So percentage is " +(countDigits*100/strlen)+"%");
		System.out.println("Number of other characters  is "+countOtherchar+". So percentage is " +(countOtherchar*100/strlen)+"%");

	}

}
