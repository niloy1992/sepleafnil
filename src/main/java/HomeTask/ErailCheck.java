package HomeTask;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class ErailCheck {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		
		driver.get("https://erail.in/");
		
		driver.manage().window().maximize();
		
		driver.findElementById("txtStationFrom").clear();
		
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("HWH", Keys.TAB);
		
		driver.findElementById("chkSelectDateOnly").click();
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		WebElement options = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		
		List<WebElement> allOptions = options.findElements(By.tagName("tr"));
		
		System.out.println(allOptions.size());
		
		for (WebElement op : allOptions) {
			
			//System.out.println(op.getText());
			
		}
		
		WebElement secRow=allOptions.get(1);
		
		List<WebElement> findElements = secRow.findElements(By.tagName("td"));
		
		System.out.println(findElements.size());
		
		System.out.println(findElements.get(1).getText());
		
          for(int k=0;k<allOptions.size();k++) {
			
			WebElement checkRow=allOptions.get(k);
			
			//System.out.println(checkRow.getText());
			List<WebElement> checkColumn= checkRow.findElements(By.tagName("td"));
			
			String text1=checkColumn.get(1).getText();
			System.out.println(text1);
			
		} 

	}

}
