package HomeTask;

import java.util.Scanner;

public class RecursionString {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the string input");
		String str=sc.nextLine();
		String str2=str.toLowerCase();
		String newStr="";
		newStr=rec(str2);
		
		System.out.println("The reversed string is " +newStr);

	}
	
	public static String rec(String str1) {
		
		if(str1.length()==1 || str1.length()==0) {
			
		return str1;
		}
		else
			
			return rec(str1.substring(1))  + str1.charAt(0);
		
		
	}

}
