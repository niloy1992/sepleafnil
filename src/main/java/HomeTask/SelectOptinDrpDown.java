package HomeTask;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class SelectOptinDrpDown {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		int c=0;
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/Dropdown.html");
		WebElement drop1=driver.findElementById("dropdown1");
		
		Select sel=new Select(drop1);
		
		sel.selectByIndex(3);
		
        WebElement drop2=driver.findElementByName("dropdown2");
		
		Select sel1=new Select(drop2);
		
		sel1.selectByVisibleText("UFT/QTP");
		
		//Thread.sleep(2000);
		
        WebElement drop3=driver.findElementById("dropdown3");
		
		Select sel2=new Select(drop3);
		
		sel2.selectByValue("3");
		
        WebElement drop4=driver.findElementByClassName("dropdown");
		
		Select sel3=new Select(drop4);
		
		List<WebElement> li=sel3.getOptions();
		
		for (WebElement we : li) {
			
			c++;
			System.out.println(li.size());
			
			if(c==li.size()-1) {
				
				we.click();
				break;
			}
			
			
		}
		System.out.println(li.size());
		
	 driver.findElementByXPath("//div[@class='example'][5]/select").sendKeys("UFT/QTP");
		
		driver.findElementByXPath("//div[@class='example'][6]/select/option[4]").click();
			

	}

}
