package HomeTask;

import java.util.Scanner;

public class ArrayDuplicateNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count=0;
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter the size of the array");
		
		int size=sc.nextInt();
		
		int arr[]=new int[size];
		
		for(int h=0;h<size;h++) {
			
			arr[h]=sc.nextInt();
		}
		
		for(int r=0;r<size;r++) {
			
			count=0;
			
			for(int g=0;g<size;g++) {
				
				if(arr[r]==arr[g]) {
					
					count++;
					
				}
				
             if(arr[r]==arr[g] && r<g) {
					
					break;
				}
				
				if(g==size-1) {
					
					if(count>1)
					
					System.out.print(arr[r] + " ");   //Printed the numbers which are duplicate
				}
				
				
			}
		}

	}

}
