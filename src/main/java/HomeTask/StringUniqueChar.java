package HomeTask;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class StringUniqueChar {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the input");
		String inp=sc.nextLine();
		String temp=inp.toLowerCase().replaceAll(" ", "");
		sc.close();
		char ch[]=temp.toCharArray();
		
		List<Character> li=new ArrayList<>();
		
		for(int k=0;k<temp.length();k++) {
			
		li.add(ch[k]);
		
		}
		
		Set<Character> set1=new LinkedHashSet<>();
		
		set1.addAll(li);
		
		for (Character char1 : set1) {
			
			System.out.print(char1);
			
		}
		

	}

}
