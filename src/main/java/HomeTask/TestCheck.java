package HomeTask;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class TestCheck {

	@BeforeSuite
	public void BeforeSuite() {

		System.out.println("Before Suite");
	}

	@org.testng.annotations.BeforeTest
	public void BeforeTest() {

		System.out.println("Before Test");
	}

	@BeforeClass
	public void BeforeClass() {

		System.out.println("Before Class");
	}

	@BeforeMethod
	public void BeforeMethod() {

		System.out.println("Before Method");
	}

	@Test
	public void check() {

		System.out.println("Check the content");
	}

	@Test
	public void check1() {

		System.out.println("Check the content1");
	}

	@AfterMethod
	public void AfterMethod() {

		System.out.println("After Method");
	}

	@org.testng.annotations.AfterClass
	public void AfterClass() {

		System.out.println("After Class");
	}
	@org.testng.annotations.AfterTest
	public void AfterTest() {
		
		System.out.println("After Test");
	}

	@org.testng.annotations.AfterSuite
	public void AfterSuite() {

		System.out.println("After Suite");
	}

}
