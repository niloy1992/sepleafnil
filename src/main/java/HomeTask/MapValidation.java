package HomeTask;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class MapValidation {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Map<String, Integer> map=new HashMap<>();
		
		map.put("Oppo", 3000);
		map.put("Samsung", 9000);
		map.put("Nokia", 6000);
		map.put("Redmi", 5000);
		map.put("Lenovo", 4000);
		map.put("Samsung", 11000);
		map.put(null, null);
		map.put("Apple", null);
		
		System.out.println(map);
		System.out.println(map.keySet());
		System.out.println(map.values());
		
		System.out.println(map.size());
		
		for (Entry<String, Integer>  ent: map.entrySet()) {
			
			System.out.println(ent);
			
		}
		

	}

}
