package HomeTask;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class PrintAllCountryName {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
			
			ChromeDriver driver=new ChromeDriver();
			driver.manage().window().maximize();
			driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
			
			WebElement country = driver.findElementById("userRegistrationForm:countries");
			
			Select sel1=new Select(country);
			
			List<WebElement> allOptions = sel1.getOptions();
			
			
			for (WebElement eachOption : allOptions) {
				
				System.out.println(eachOption.getText());
				
			}
			
			System.out.println(allOptions.size());
			
			

	}

}
