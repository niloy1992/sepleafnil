package HomeTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeCheck {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByXPath("(//ul[@class='shortcuts']/li)[4]/a").click();
		driver.findElementByXPath("//img[@alt='Lookup']").click();
		
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		Set<String> handle= driver.getWindowHandles();
		
		List<String> windList=new ArrayList<>();
		windList.addAll(handle);
		
		driver.switchTo().window(windList.get(1));
		
		driver.findElementByName("firstName").sendKeys("testing1");
		driver.findElementByXPath("//button[@class='x-btn-text']").click();
		//driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.findElementByPartialLinkText("10872").click();
		
		
		driver.switchTo().window(windList.get(0));
		
		
		driver.findElementByXPath("(//img[@alt='Lookup'])[2]").click();
		
     Set<String> handle1= driver.getWindowHandles();
		
		List<String> windList1=new ArrayList<>();
		windList1.addAll(handle1);
		
		driver.switchTo().window(windList1.get(1));
		
		driver.findElementByName("firstName").sendKeys("Test1");
		driver.findElementByXPath("//button[@class='x-btn-text']").click();
		driver.findElementByPartialLinkText("10412").click();
	
		driver.switchTo().window(windList1.get(0));
		
		
		driver.findElementByLinkText("Merge").click();
		
		driver.switchTo().alert().accept();
		
		driver.findElementByXPath("//ul[@class='shortcuts']/li[3]/a").click();
		
		driver.findElementByXPath("( //input[@name='firstName'])[3]").sendKeys("testing1");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		
		WebElement text = driver.findElementByXPath("//div[text()='No records to display']"); 
		
		System.out.println(text.getText());
		
		driver.close();
		

	}

}
