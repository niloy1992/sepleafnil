package HomeTask;

import java.util.Scanner;

public class PatternPrint {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int count=0;
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the rows you want to print: ");
		int rows=sc.nextInt();
		
		for(int i=1;i<=rows;i++) {
			
			for(int k=1;k<=i;k++) {
				
				count++;
				System.out.print(count + " ");
			}
			System.out.println();
		}

	}

}
