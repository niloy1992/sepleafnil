package HomeTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class DigitAscending {

	public static void main(String[] args) {
		int i = 0;
		int newNo = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the number :");
		int num = sc.nextInt();
		int temp = num;
		String str = String.valueOf(num);
		int size = str.length();
		sc.close();
		int arr[] = new int[size];

		while (temp > 0) {

			arr[i++] = temp % 10;
			temp = temp / 10;

		}

		Set<Integer> se1 = new TreeSet<>();

		for (int k = 0; k < size; k++) {

			se1.add(arr[k]);

		}

//        for (Integer num1 : se1) {
//        	
//        	System.out.print(num1);   //for storing the new number in set using these method
//			
//		}

		List<Integer> li = new ArrayList<>();
		li.addAll(se1);

		for (int p = 0; p < li.size(); p++) {
			newNo = newNo * 10 + li.get(p);      //for storing the result in a no
		}

		System.out.println(newNo);
	}

}
