package HomeTask;

import java.util.Scanner;

public class RecursionNumber {
	public static int rem = 0;
	public static int sum = 0;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the input number");
		int inp = sc.nextInt();

		System.out.println(reverse(inp));

	}

	public static int reverse(int num) {
		
		if(num!=0) {
			
			rem=num%10;
			sum=sum*10+rem;
			reverse(num/10);
			
		}
		
		else {
			
			return num;
		}
		
		
		return sum;		
	}

}
