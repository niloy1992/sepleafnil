package HomeTask;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckboxSelection {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		int c=0;
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leafground.com/pages/checkbox.html");
		driver.findElementByXPath("//div[@class='example']/input").click();
		WebElement ConfirmSelenium=driver.findElementByXPath("(//div[@class='example']/input)[6]");
		
		if(ConfirmSelenium.isSelected()) {
			
			System.out.println("Selenium is selected");
		}

		else
			
			ConfirmSelenium.click();
		
		WebElement NotSelected = driver.findElementByXPath("(//div[@class='example']/input)[7]");
		
		if(NotSelected.isSelected()) {
			
			NotSelected.click();
		}
		
		else
			
			System.out.println("The Not Selected checkbox is by default not checked");
		
		WebElement IsSelected = driver.findElementByXPath("(//div[@class='example']/input)[8]");
		
        if(IsSelected.isSelected()) {
			
        	IsSelected.click();
		}
		
		else
			
			System.out.println("Already deselected");
        
        
        driver.findElementByXPath("(//div[@class='example']/input)[9]").click();
        driver.findElementByXPath("(//div[@class='example']/input)[10]").click();
        driver.findElementByXPath("(//div[@class='example']/input)[11]").click();
        driver.findElementByXPath("(//div[@class='example']/input)[12]").click();
        driver.findElementByXPath("(//div[@class='example']/input)[13]").click();
        driver.findElementByXPath("(//div[@class='example']/input)[14]").click();
       

	}
	
	

}
