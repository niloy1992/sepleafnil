package HomeTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class WindowHandlingConcept {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
			  System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
	
					ChromeDriver driver=new ChromeDriver();
					driver.manage().window().maximize();
					driver.get("http://seleniumpractise.blogspot.com/");
					driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
					driver.findElementByLinkText("Multiple window examples").click();
					driver.findElementByXPath("//div[@class='post-body entry-content']/a").click();
					
					driver.findElementByXPath("//div[@class='post-body entry-content']/a[2]").click();
					driver.findElementByXPath("//div[@class='post-body entry-content']/a[3]").click();
					//driver.findElementByXPath("//div[@class='post-body entry-content']/a[4]").click();
					
					Set<String> allWindows = driver.getWindowHandles();
					
					List<String> ls1=new ArrayList<>();
					
					ls1.addAll(allWindows);
					System.out.println(ls1.size());
					Thread.sleep(4000);
					driver.switchTo().window(ls1.get(0));
					System.out.println(driver.getTitle());
					Thread.sleep(4000);
					driver.switchTo().window(ls1.get(1));
					System.out.println(driver.getTitle());
					driver.switchTo().window(ls1.get(2));
					System.out.println(driver.getTitle());
					driver.switchTo().window(ls1.get(3));
					System.out.println(driver.getTitle());

	}

}
