package week1.day1;

import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first no");
		float val1=sc.nextInt();
		System.out.println("Enter the second no");
		float val2=sc.nextInt();
		System.out.println("Enter the operation to be performed");
		String operation1=sc.next();
		sc.close();
		String operation= operation1.toUpperCase();
		
		switch(operation) {
		case "ADD":
			System.out.println("Add two numbers" + (val1+val2));
			break;
			
		case "SUBTRACT":
			System.out.println("Subtract two numbers" + (val1-val2));
			break;
			
		case "MULTIPLY":
			System.out.println("Multiply two numbers" + (val1*val2));
			break;
			
		case "DIVIDE":
			if(val2==0){
				System.out.println("Divide not successful");
			}
			else {
				float k=val1/val2;
			System.out.println("Divide two numbers" + k);
			}
			break;	
			
		default:
			System.out.println("Invalid input");
			break;
			
		}

	}

}
