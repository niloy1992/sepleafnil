package week1.day1;

public class SmartTV {
	
	public String brand;
	protected String brightness="Good quality";
	public int soundLevel;
	
	public void brand(String brand) {
		
		System.out.println("The brand name is " + brand);
	}
	
	public void sound(int soundLevel) {
		
		System.out.println("The sound level is "+ soundLevel);
	}
	
	public void colour() {
		
		System.out.println("The pic quality is " + brightness);
	}

}
