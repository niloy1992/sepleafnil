package week1.day1;

import java.util.Scanner;

public class MobileNumberProvider {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the first three digit of the mobile no");
		int firstThreeDigit=sc.nextInt();
		sc.close();
		
		if(firstThreeDigit==944)
			System.out.println("The service provider is BSNL");
			
		else if(firstThreeDigit==900)
			System.out.println("The service provider is Airtel");
			
		else if(firstThreeDigit==897)
			System.out.println("The service provider is Idea");
			
		else if(firstThreeDigit==630)
				System.out.println("The service provider is JIO");
		
		else
			System.out.println("Not a valid service provider");
		

	}

}
