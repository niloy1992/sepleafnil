package week1.day1;

import java.util.Scanner;

public class Costliest_Cheapest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the cost of the first mobile");
		int cost1=sc.nextInt();
		System.out.println("Enter the cost of the second mobile");
		int cost2=sc.nextInt();
		sc.close();
		
		if(cost1>cost2)  {
			
			//System.out.println("The first mobile is costlier than second mobile");
			System.out.println("The price of the costlier mobile is " +cost1 +" & the price of the cheaper mobile is " + cost2 );
		}
		else if(cost1<cost2) {
			
			//System.out.println("The second mobile is costlier than second mobile");
			System.out.println("The price of the costlier mobile is " +cost2 +" & the price of the cheaper mobile is " + cost1 );
		}
		
		else 
			System.out.println("Prices of both the mobiles are same");
		

	}

}
