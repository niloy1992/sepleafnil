package week1.day2;

import java.util.Scanner;

public class SumOddDigit {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int rem=0;
		int sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		
		int inp=sc.nextInt();
		sc.close();
		while(inp>0) {
			
			rem=inp%10;
			if(rem%2!=0) {
				
				sum+=rem;
			}
			
			inp/=10;
			
		}
		
		System.out.println(sum);

	}

}
