package week1.day2;

import java.util.Scanner;

public class DemoArray {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sum=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array");
		int size=sc.nextInt();
		int [] arr=new int[size];
		System.out.println("Enter the numbers");
		//sc.close();
		for(int i=0;i<arr.length;i++) {
			
			arr[i]=sc.nextInt();
		}
		
		//only used for printing values & not for reading values
		sc.close();
		for(int sal: arr) {
		 sum=sum+ sal;
			
			System.out.println(sal);
		}
		
		/*for(int j=0;j<arr.length;j++) {
			
			//sum=sum+ arr[j];
			
			System.out.println(arr[j]);
		}*/
		System.out.println(sum);

	}

}
