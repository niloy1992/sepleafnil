package week1.day2;

import java.util.Scanner;

public class Odd_Even_Sum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int sumEven=0;
		int sumOdd=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of the array");
		int size=sc.nextInt();
		int [] arr=new int[size];
		System.out.println("Enter the numbers");
		
		for(int i=0;i<arr.length;i++) {
			
			arr[i]=sc.nextInt();
		}
		sc.close();
		
		for(int j=0;j<arr.length;j++) {
			
			if(arr[j]%2==0) {
				
				sumEven=sumEven+arr[j];
			}
			else
				
				sumOdd=sumOdd+arr[j];
		}
		
		System.out.println("Sum of odd numbers "+ sumOdd);
		System.out.println("Sum of even numbers "+ sumEven);

	}

}
