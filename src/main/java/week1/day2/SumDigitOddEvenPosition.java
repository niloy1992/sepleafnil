package week1.day2;

import java.util.Scanner;

public class SumDigitOddEvenPosition {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int count=0;
		int rem=0;
		int sumOddPosition=0;
		int sumEvenPosition=0;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the number");
		int num=sc.nextInt();
		int temp=num;
		sc.close();
		
		while(num>0) {
			
			count++;
//			num/=10;
			num=num/10;
		}
		
		//System.out.println(count);
		
		while(temp>0) {
			rem=temp%10;
			if(count%2!=0) {
				
				sumOddPosition+=rem;
			}
			else {
				sumEvenPosition+=rem;
			}
			
		count--;
		temp/=10;
			
		}
		
		System.out.println(sumOddPosition);
		System.out.println(sumEvenPosition);
	}

}
