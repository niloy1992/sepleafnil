package week3.day2;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Erail {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://erail.in");
		
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		
		
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("MDU",Keys.TAB);
		
		WebElement ele=driver.findElementById("chkSelectDateOnly");
		
		if(ele.isSelected()) {
			
			ele.click();
		}
		
		Thread.sleep(3000);
		
		WebElement  table=driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> allRows=table.findElements(By.tagName("tr"));
		System.out.println(allRows.size());
		//Second row
//		WebElement secRow=allRows.get(1);
//		//No of colums in 2nd row
//	   List<WebElement> allColumn= secRow.findElements(By.tagName("td"));
//	   
//	   System.out.println(allColumn.size());
//	   //Train name in 2nd row
//	   String text=allColumn.get(1).getText();
//	   System.out.println(text);
//		
//		
		
		for(int k=0;k<allRows.size();k++) {
			
			WebElement checkRow=allRows.get(k);
			
			//System.out.println(checkRow.getText());
			List<WebElement> checkColumn= checkRow.findElements(By.tagName("td"));
			
			String text1=checkColumn.get(1).getText();
			System.out.println(text1);
			
		}
		
		
		

	}

}
