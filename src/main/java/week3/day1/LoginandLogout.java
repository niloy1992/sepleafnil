package week3.day1;



import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class LoginandLogout {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		
		driver.findElementByClassName("decorativeSubmit").click();
		
		driver.findElementByLinkText("CRM/SFA").click();
		
		driver.findElementByLinkText("Create Lead").click();
//		driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
//		driver.findElementById("createLeadForm_firstName").sendKeys("Test1");
//		driver.findElementById("createLeadForm_lastName").sendKeys("Test2");
//		driver.findElementByClassName("smallSubmit").click();
		
		WebElement source =driver.findElementById("createLeadForm_dataSourceId");
		Select sel=new Select(source);
		sel.selectByVisibleText("Public Relations");
		
		WebElement marketCampaign=driver.findElementById("createLeadForm_marketingCampaignId");
		Select sel1=new Select(marketCampaign);
		sel1.selectByValue("CATRQ_AUTOMOBILE");
		
		WebElement industry=driver.findElementById("createLeadForm_industryEnumId");
		Select sel2=new Select(industry);
		sel2.selectByIndex(2);
		List<WebElement> ind1=sel2.getOptions();
		for(WebElement we: ind1) {
			
			if((we.getText().startsWith("M"))) {
				
				System.out.println(we.getText());
			}
			
			//System.out.println(we.getText());
			
			
		}
		System.out.println(ind1.size());
		
		
		
		
		

}
}
