package week3.day1;

import java.util.List;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
//import org.openqa.selenium.support.ui.Select;

public class IRCTC_Login {

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
        System.setProperty("webdriver.chrome.driver", "./driver/chromedriver.exe");
		
		ChromeDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		
		driver.findElementById("userRegistrationForm:userName").sendKeys("test190");
		driver.findElementByLinkText("Check Availability").click();
		driver.findElementById("userRegistrationForm:password").sendKeys("nlq299");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("nlq299");
		
		//userRegistrationForm:confpasword
		
		WebElement secQues=driver.findElementById("userRegistrationForm:securityQ");
		
		Select s=new Select(secQues);
		s.selectByIndex(3);
		
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Chanding");
		Thread.sleep(3000);
		
		WebElement preflang=driver.findElementById("userRegistrationForm:prelan");
		Select s1=new Select(preflang);
		s.selectByIndex(0);
		
		
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Tested");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Kumar");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("John");
		
		WebElement date=driver.findElementById("userRegistrationForm:dobDay");
		Select s2=new Select(date);
		s2.selectByIndex(2);
		
		WebElement month=driver.findElementById("userRegistrationForm:dobMonth");
		Select s3=new Select(month);
		s3.selectByIndex(5);
		
		WebElement year=driver.findElementById("userRegistrationForm:dateOfBirth");
		Select s4=new Select(year);
		s4.selectByIndex(8);
		
		WebElement occupation=driver.findElementById("userRegistrationForm:occupation");
		Select s5=new Select(occupation);
		s5.selectByIndex(6);
		
		driver.findElementById("userRegistrationForm:uidno").sendKeys("4536736383837638");
		driver.findElementById("userRegistrationForm:idno").sendKeys("bzjer5437u");
		
		WebElement country1=driver.findElementById("userRegistrationForm:countries");
		Select s6=new Select(country1);
		s6.selectByValue("94");
		
		
		driver.findElementById("userRegistrationForm:email").sendKeys("xyz@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("7412584974");
		
		WebElement nationality=driver.findElementById("userRegistrationForm:nationalityId");
		Select s7=new Select(nationality);
		s7.selectByValue("94");
		
		
		driver.findElementById("userRegistrationForm:address").sendKeys("10A/1");
		driver.findElementByName("userRegistrationForm:street").sendKeys("Sagar manna");
		driver.findElementByName("userRegistrationForm:area").sendKeys("Behala");
		
		driver.findElementByName("userRegistrationForm:pincode").sendKeys("700060",Keys.TAB);
		
		Thread.sleep(2000);
		
		WebElement city=driver.findElementById("userRegistrationForm:cityName");
		Select s8=new Select(city);
		s8.selectByValue("Kolkata");
		
		Thread.sleep(2000);
		
		WebElement po=driver.findElementById("userRegistrationForm:postofficeName");
		Select s9=new Select(po);
		s9.selectByValue("Mahendra Banerjee Road S.O");
		
		driver.findElementByName("userRegistrationForm:landline").sendKeys("03324015269");
			

	}

}
