package utils;

import java.io.IOException;

import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public abstract class Report {
	public static ExtentHtmlReporter html;
	public static ExtentReports extent;
	public static ExtentTest logger;
    public String testCaseName, testDesc, author, category;
    
    @BeforeSuite(groups= {"any"})
	   public void startResult() {
		    html=new ExtentHtmlReporter("./report/result.html");
			html.setAppendExisting(true);
			extent=new ExtentReports();
			extent.attachReporter(html);
		//ExtentTest Logger= extent.createTest(testName);
	   }
		
	   public void reportStep(String status, String desc)  {
			if (status.equalsIgnoreCase("pass")) {
				logger.log(Status.PASS, desc);			
			} else if (status.equalsIgnoreCase("fail")) {
				logger.log(Status.FAIL, desc);			
			}
		}
		
		//@AfterSuite
		public void endResult() {
	     extent.flush();
		}
		
		//@BeforeMethod
				public void beforeMethod() {
					logger=extent.createTest(testCaseName, testDesc);
					logger.assignAuthor(author);
					logger.assignCategory(category);	
				}
	}


